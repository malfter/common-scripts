#!/usr/bin/env bash

# Configure your paths
SOURCEBINPATH=src/bin
SOURCEMANPATH=src/man
SOURCEBASHCOMPLPATH=src/etc/bash_completion.d
SOURCEZSHCOMPLPATH=src/usr/share/zsh/vendor-completions

DEBFOLDER=team-commons
DEBVERSION=0.0.2
export DEBEMAIL="support@alfter-web.de"
export DEBFULLNAME="Markus Alfter"

DEBFOLDERNAME=$DEBFOLDER-$DEBVERSION
DEBBUILDFILESPREFIX=${DEBFOLDER}_${DEBVERSION}

command -v help2man &> /dev/null || { echo "Missing command 'help2man', please install 'apt install help2man'"; exit 1; }
command -v debuild &> /dev/null  || { echo "Missing command 'debuild', please install 'apt install devscripts build-essential lintian'"; exit 1; }
command -v dh_make &> /dev/null  || { echo "Missing command 'dh_make', please install 'apt install dh-make'"; exit 1; }

if [ "$DEBIAN_FRONTEND" != "noninteractive" ]; then
    echo "Start packaging ''$DEBFOLDERNAME'..."
    while true; do
        # shellcheck disable=SC2162
        read -p "Are all changes documented? ('debian/{changelog,control}') y/n" yn
        case $yn in
            [Yy]* ) break;;
            [Nn]* ) exit;;
            * ) echo "Please answer yes or no.";;
        esac
    done
fi

# Cleanup old build
rm -rf $DEBFOLDERNAME $DEBBUILDFILESPREFIX*

# Create your source directories
mkdir -p $DEBFOLDERNAME/{bin,man,bash_completion,zsh_completion}

# Generate man page
help2man --no-info --include $SOURCEMANPATH/dbssh_additional_text.txt -o $DEBFOLDERNAME/man/dbssh.1 $SOURCEBINPATH/dbssh
gzip $DEBFOLDERNAME/man/dbssh.1

# Copy your files to the source dir
cp $SOURCEBINPATH/* $DEBFOLDERNAME/bin
cp $SOURCEBASHCOMPLPATH/* $DEBFOLDERNAME/bash_completion
cp $SOURCEZSHCOMPLPATH/* $DEBFOLDERNAME/zsh_completion
cd $DEBFOLDERNAME || { echo "change dir 'cd $DEBFOLDERNAME' fails"; exit 1; }

# Create the packaging skeleton (debian/*)
if [ "$DEBIAN_FRONTEND" == "noninteractive" ]; then
    DH_MAKE_NONINTERACTIVE="--yes"
fi
dh_make --indep --createorig "$DH_MAKE_NONINTERACTIVE"

# Override dh_make template files
cp ../debian/* debian

# Remove/Cleanup unused files (debian/*)
rm debian/README.Debian debian/README.source
rm debian/team-commons-docs.docs
echo "" > debian/copyright

# Remove 'make' command calls
grep -v makefile debian/rules > debian/rules.new
mv debian/rules.new debian/rules
chmod +x debian/rules

# Generate 'debian/install' file, must contain the list of files to install
for f in bin/*;
do
 echo "$f" usr/bin/ >> debian/install
done
for f in man/*;
do
 echo "$f" usr/share/man/man1/ >> debian/install
done
for f in bash_completion/*;
do
 echo "$f" usr/share/bash-completion/completions >> debian/install
done
for f in zsh_completion/*;
do
 echo "$f" usr/share/zsh/vendor-completions >> debian/install
done

# Remove the example files
rm debian/*.ex
rm debian/*.EX

# Build the package.
# Using these flags tells debuild to proceed without GPG signing any .changes or .dsc files.
debuild -us -uc

echo "Finished packaging ''$DEBFOLDERNAME'"
