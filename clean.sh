#!/bin/bash

set -e

# generated source folder
rm team-commons-* -rf

# generated build artifacts
rm team-commons_* -rf

exit 0
