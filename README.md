# common scripts

[![pipeline status](https://gitlab.com/malfter/common-scripts/badges/master/pipeline.svg)](https://gitlab.com/malfter/common-scripts/-/commits/master)

![Logo](./logo.png)
Logo erstellt von <a href="https://www.flaticon.com/de/autoren/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/de/" title="Flaticon"> www.flaticon.com</a>

Bei der täglichen Arbeit werden oft wiederkehrende oder fehleranfällige manuelle Arbeiten durch Scripte automatisiert.

Leider finden diese Scripte oft nicht den Weg in das Team. Oder Sie werden einmalig geteilt und dann unabhängig von einander weiter entwickelt.

Im Rahmen des m-bs Freiraums ist dieses Projekt entstanden. Folgende Punkte haben mich dazu bewogen dieses Projekt zu starten:

 * Es sollte eine Anlaufstelle geben, Scripte im Team zur Verfügung zu stellen.
 * Durch Erweiterung und Verbesserung der Scripte im Team werden die Scripte kontinuierlich besser.
 * Diskussionen über wiederkehrende Arbeiten im Team können zu anderen Lösungswegen führen.

## Projektziel

Im Rahmen des m-bs Freiraums habe ich mir folgende Projektziele gesetzt:

* Bereitstellung eines Repositories zur Sammlung von Scripten
* Erstellung/Bereitstellung eines Scripts das als Vorlage für weitere Scripte dienen kann, hier sollten folgende Aspekte behandelt sein:
  * Script sollte mit einer ManPage ausgeliefert werden.
  * Behandlung und Prüfung von Argumenten.
  * Abbrüche sollten mit klaren Fehlermeldungen erfolgen.
  * Autovervollständigung von Parametern und Argumenten
* Die Script Sammlung soll über ein Debian Package bereitgestellt werden.
* CI-Build des Debian Package

Weiterführende Punkte die betrachtet wurden aber aktuell nicht umgesetzt wurden:
* Bereitstellung des Packages in einem APT Repository

## Quality Gate zur Aufnahme weiterer Scripte

* Script muss eine Hilfe oder ManPage bereitstellen in dem die Nutzung und alle Argumente erklärt sind.

## Projektstruktur

* debian (source package control files)
  * changelog (explained changes in package)
  * control (informations other source package)
* src
  * bin (directory for scripts)
  * etc (directory for bash completions)
  * man (directory for ManPages)

## Bereitstellung und Installation des Debian Package

```bash
# Package team-commons
$ ./package.sh
```

```bash
$ dpkg -i team-commons-<version>.deb

# Wird die Installation abgebrochen weil Abhängigkeiten nicht installiert sind,
# können diese Abhängigkeiten automatich aufgelöst und installiert werden
$ apt-get install -f
```
