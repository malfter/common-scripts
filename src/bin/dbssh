#!/usr/bin/env sh
# POSIX

DEBUG=false

_log() {
  if [ "$TERM" = "xterm-256color" ]; then
    textRed=$(tput setaf 1)
    textYellow=$(tput setaf 3)
    textNormal=$(tput sgr0)
  fi

  # example: _log "DEBUG" "This is a debug Text"
  if "$DEBUG" && [ "${1}" = "DEBUG" ]; then
    printf '%-7s %s\n' "[$1]" "$2"
    return 0
  elif [ "${1}" = "WARN" ]; then
    printf '%-7s %s\n' "${textYellow}[$1]${textNormal}" "$2" >&2
    return 0
  elif [ "${1}" = "ERROR" ]; then
    printf '%-7s %s\n' "${textRed}[$1]${textNormal}" "$2" >&2
    exit 1
  fi
}

die() {
    _log ERROR "$1"
    exit 1
}

show_version() {
cat <<EOF
$(basename "$0") version 1.0
EOF
}

show_usage() {
cat <<EOF
Usage: $(basename "$0") [-c db-config] hostname
   or: $(basename "$0") [-h] | [-l] | [-V]
EOF
}

show_help() {
cat <<EOF
'$(basename "$0")' Connects to a host using ssh with port forwarding and opens the database specific management tool.

$(show_usage)

Options:
 -c, --config       Specify config to be used for db connection
 -l, --list         List all db configurations
 -h, --help         display this help and exit
 -V, --version      output version information and exit
 -v, --verbose      verbose mode

Examples:
 $(basename "$0") -c mongo my.example.com

EOF
}

show_config_error() {
cat <<EOF
No configurations found. Please make sure a configuration file exists ('~/.dbssh.conf').

Example:
$ cat ~/.dbssh.conf

#<config_name>,<local_port>,<destination_port>,<db_mgmt_tool>
mongo,27018,27017,robo3t
EOF
}

list_configs() {
 if [ -f ~/.dbssh.conf ]; then
  cat ~/.dbssh.conf
 else
  show_config_error
 fi
}

# Initialize all the option variables.
# This ensures we are not contaminated by variables from the environment.
config=

while :; do
    case $1 in
        -h|-\?|--help)
            show_help	# Display a usage synopsis.
            exit
            ;;
        -l|--list)
            list_configs
            exit
            ;;
        -c|--config)	# Takes an option argument; ensure it has been specified.
            if [ "$2" ]; then
                config=$2
                shift
            else
                die 'ERROR: "--config" requires a non-empty option argument.'
            fi
            ;;
        --config=?*)
            config=${1#*=} # Delete everything up to "=" and assign the remainder.
            ;;
        --config=)         # Handle the case of an empty --file=
            die 'ERROR: "--config" requires a non-empty option argument.'
            ;;
        -V|--version)
            show_version
            exit
            ;;
        -v|--verbose)
            DEBUG=true
            shift
            break
            ;;
        --)              # End of all options.
            shift
            break
            ;;
        -?*)
            _log WARN "Unknown option (ignored): $1"
            ;;
        *)               # Default case: No more options, so break out of the loop.
            #show_usage
            break
    esac

    shift
done

destination=$1
[ "x" = "x${destination}" ] && die "No destination found, please enter a host!"
[ "x" = "x${config}" ]      && die "No config found, please enter a configuration (-c CONFIG_NAME)!"

# Rest of the program here.
# If there are input files (for example) that follow the options, they
# will remain in the "$@" positional parameters.

# Find configuration
[ -f ~/.dbssh.conf ] && configuration_line=$(grep "^${config}," ~/.dbssh.conf)
[ "x" = "x${configuration_line}" ] && die "Configuration '${config}' not found! (List available configurations with '$(basename "$0") -l')"
_log DEBUG "configuration found: '${configuration_line}'"

# Extract configuration values
local_port=$(echo "$configuration_line" | cut -d',' -f2)
destination_port=$(echo "$configuration_line" | cut -d',' -f3)
db_mgmt_tool=$(echo "$configuration_line" | cut -d',' -f4)

eval "${db_mgmt_tool} &"
slogin -L:"${local_port}":127.0.0.1:"${destination_port}" "$destination"
